#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <numeric>
#include <vector>
#include <array>
#include <algorithm>

using namespace std;

int pollig(int, int);

bool isPrime(unsigned long a){
    unsigned long i;
    if (a == 2)
        return true;
    if (a == 0 || a == 1 || a % 2 == 0)
        return false;
    for(i = 3; i*i <= a && a % i; i += 2)
        ;
    return i*i > a;
}

int main() {
    srand(time(nullptr));
    ifstream fin;
    ofstream fout;
    fout.open("output.txt"); //
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') { //ошибка если есть другие знаки, кроме чисел и пробелов
            isErrorLine = true;
        }
        if(tmp == ' ' && !isErrorLine){
            readingNumber++;
            if(readingNumber > 1){ //проверка на длину строки (в кол-ве чисел)
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && readingNumber == 1 && a[1] < a[0] && isPrime(a[0])){ //тут надо проверить на правильность все числа
                int result = pollig(a[0], a[1]);
                if(result != -1){
                    fout<<result<<endl;
                }
            }
            isErrorLine = false;
            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1 && !isErrorLine){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

int phi (int n) {
    int result = n;
    for (int i=2; i*i<=n; ++i)
        if (n % i == 0) {
            while (n % i == 0)
                n /= i;
            result -= result / i;
        }
    if (n > 1)
        result -= result / n;
    return result;
}

int pow(int x, int y, int n){
    while(y < 0){
        y += phi(n);
    }
    int k = 1;
    for(int l = 1; l<= y; l++){
        k = k * x % n;
    }
    return k;
}

vector<int> factor(int p){
    vector<int> d;
    if(p % 2 == 0){
        d.push_back(2);
        while(p % 2 == 0) p /= 2;
    }
    for(int i = 3; i <= p && p != 1; i += 2){
        if(p % i == 0){
            d.push_back(i);
            while(p % i == 0) p /= i;
        }
    }
    return d;
}

vector<int> getE(int p, const vector<int>& factors){
    vector<int> e;
    for(auto d : factors){
        e.push_back(0);
        while(p % d == 0){
            e[e.size()-1]++;
            p /= d;
        }
    }
    return e;
}

int findGenerator(int n){
    auto d = factor(n-1);
    for(int i = 2; i<n; i++){
        bool isGenerator = true;
        for(int q : d){
            if(pow(i, ((n-1)/q), n) % n == 1) isGenerator = false;
        }
        if(isGenerator) return i;
    }
    return 0;
}

int findX0(int a, int n, int p, const vector<vector<int>>& r, int i){
    int b = pow(a, (n-1)/p, n);
    for(int j = 0; j < p; j++){
        if(b == r[i][j]){
            return j;
        }
    }
    return -1;
}

int findXj(int a, int g, int y, int p, int i, int j, const vector<vector<int>>& r, int n){
    int b = pow(a*pow(g, -y, n), n/(pow(p, j+1)), n);
    cout<<"b = "<<b<<" y = "<<y<<" g^-y = "<<pow(g, -y, n)<<endl;
    for(int k = 0; k < p; k++){
        if(b == r[i][k]){
            return k;
        }
    }
    return -1;
}

int pollig(int n, int a) {
    auto p = factor(n - 1);
    int g = findGenerator(n);
    //построение таблиц
    cout<<"n = "<<n<<", a = "<<a<<", g = "<<g<<endl;

    vector<vector<int>> r(p.size(), vector<int>(0));
    vector<int> alpha;
    for(int i = 0; i < p.size(); i++){
        alpha.push_back(pow(g, (n-1) / p[i], n));
        for(int j = 0; j < p[i]; j++){
            r[i].push_back(pow(alpha[i], j, n));
        }
    }
    for(int i = 0; i < p.size(); i++){
        cout<<p[i]<<" "<<alpha[i]<<" : ";
        for(int j = 0; j < p[i]; j++){
            cout<<r[i][j]<<", ";
        }
        cout<<endl;
    }
    vector<int> x(p.size(), 0);
    auto e = getE(n-1, p);
    for(int i = 0; i < p.size(); i++){
        int y = 0;
        int x0 = findX0(a, n, p[i], r, i);
        y += x0;
        cout<<"x0 = "<<x0<<endl;
        for(int j = 1; j < e[i]; j++){
            int x = findXj(a, g, y, p[i], i, j, r, n);
            y += x * pow(p[i], j);
            cout<<"x = "<<x<<endl;
        }
        cout<<"y = "<<y<<endl;
        x[i] = y;
    }

    for(int i = 0; i<x.size(); i++){
        cout<<" x == "<<x[i]<<" mod "<<pow(p[i], e[i])<<endl;
    }
    int res = 0;
    for(int i = 0; i < p.size(); i++){
        int m = pow(p[i], e[i]);
        int M = (n-1)/m;
        int Ms = pow(M, -1, m);
        cout<<"m = "<<m<<" M = "<<M<<" Ms = "<<Ms<<endl;
        res += x[i] * M * Ms % (n-1);
    }
    res %= n-1;
    cout<<res<<endl;
    return res;
}