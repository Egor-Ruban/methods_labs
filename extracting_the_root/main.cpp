#include <iostream>
#include <fstream>

using namespace std;

unsigned int extracting_the_root(unsigned int, unsigned int);

int main() {
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    int a[]{0,0}; //массив для чисел из строки
    int readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') {
            isErrorLine = true;
        }
        if(tmp == ' '){
            readingNumber++;
            if(readingNumber > 1){
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine){
                unsigned int result = extracting_the_root(a[0], a[1]);
                fout << result << endl;
            }
            isErrorLine = false;

            for(int & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

int from_left_to_right(unsigned int x, unsigned int y, unsigned int m){
    int z = x;
    int t = 2;
    int n = 0;
    for(; y >= t;  n++) t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z) % m;
        if (((y >> i) & 1) == 1) z = (z * (x % m)) % m;
    }
    return z;
}


//считаем в четверичной системе
unsigned int extracting_the_root(unsigned int a, unsigned int n){
    unsigned int a0 = a;
    unsigned int a1 = a;
    do {
        a0 = a1;
        a1 = ((a/from_left_to_right(a0, n-1, -1))+(n-1)*a0)/n;
    } while (a0 > a1);
    return a0;
}
