#include <iostream>
#include <fstream>

using namespace std;

string trial_division(unsigned long long, unsigned long long) ;

int main() {

    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') { //ошибка если есть другие знаки, кроме чисел и пробелов
            isErrorLine = true;
        }
        if(tmp == ' '){
            readingNumber++;
            if(readingNumber > 1){ //проверка на длину строки (в кол-ве чисел)
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && readingNumber == 1){ //тут надо проверить на правильность все числа
                string result = trial_division(a[0], a[1]);
                fout << result ;
            }
            isErrorLine = false;
            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

string trial_division(unsigned long long n, unsigned long long B) {
    unsigned long long temp = n;
    int t = 0;
    string res = "";
    //сначала проверить на четность. Если четное то убираем все двойки
    for (; n % 2 == 0; n = n / 2) res += "2 ";
    //первый пробный делитель - тройка
    unsigned long long d = 3;
    //если dk > B, то факторизация окончена
    while(n != 1 && d <= B){
        unsigned long long q = n / d;
        unsigned long long r = n % d;
        if (r == 0){
            res = res + to_string(d) + " ";
            n = q;
        } else if (d < q){
            d += 2;
        } else {
            if(n == temp) t++;
            res = res + to_string(n) + " ";
            n = 1;
        }
    }
    res += to_string(n) + " ";
    res = res + ((n > 1) ? "1" : "0" ) + "\n";
    //if (t == 1) res = "";

    //в выводе строка - делители в порядке возрастания, остаток, флаг.
    // Если факторизация закончена, то флаг 0, если не закончена то флаг 1
    return res;

    //если простое, то ничего не выводить

}