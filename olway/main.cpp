#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

string olway(long long);

int main() {
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') { //ошибка если есть другие знаки, кроме чисел и пробелов
            isErrorLine = true;
        }
        if(tmp == ' ' && !isErrorLine){
            readingNumber++;
            if(readingNumber > 0){ //проверка на длину строки (в кол-ве чисел)
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && readingNumber == 0){ //тут надо проверить на правильность все числа
                //сначала проверить на четность. Если четное то убираем все двойки
                for (; a[0] % 2 == 0; a[0] /= 2);
                string result = olway(a[0]);
                fout<<result;
            }
            isErrorLine = false;
            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1 && isErrorLine == false){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

string olway(long long n) {
    string res = "";
    long long d = 2 * cbrt(n) + 1;
    if (d % 2 == 0) d++;

    long long r1 = n % d;
    long long r2 = n % (d-2);
    long long q = 4 * (floor(n/(d-2)) - floor(n/d));
    long long s = floor(sqrt(n));
    //если d > s, то ничего не выводить
    second:
    d += 2;
    if (d > s){
        return "";
    }

    long long r = 2 * r1 - r2 + q;
    if (r < 0){
        r = r + d;
        q = q + 4;
    } else if (r >= d) {
        do{
           //cout<<"hey2"<<endl;
            r = r - d;
            q = q - 4;
        } while(r >= d);
    }
    if (r == 0) return to_string(d) + "\n";
    else {
        r2 = r1;
        r1 = r;
        goto second;
    }
    //если простое, то ничего не выводить

    return res;
}

