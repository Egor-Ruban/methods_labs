#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <numeric>
#include <vector>
using namespace std;

int dixon(long long, long long);

bool isPrime(unsigned long a){
    unsigned long i;
    if (a == 2)
        return true;
    if (a == 0 || a == 1 || a % 2 == 0)
        return false;
    for(i = 3; i*i <= a && a % i; i += 2)
        ;
    return i*i > a;
}

int main() {
    srand(time(nullptr));
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') { //ошибка если есть другие знаки, кроме чисел и пробелов
            isErrorLine = true;
        }
        if(tmp == ' ' && !isErrorLine){
            readingNumber++;
            if(readingNumber > 1){ //проверка на длину строки (в кол-ве чисел)
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            unsigned long long s = floor(sqrt(a[0]));
            if(!isErrorLine && readingNumber == 1 && a[1] >= 2 && a[1] <= s && !isPrime(a[0])){ //тут надо проверить на правильность все числа
                //как-то отфильтровать степени надо. нельзя подавать в алгоритм степени
                for (; a[0] % 2 == 0; a[0] /= 2);
                int result = dixon(a[0], a[1]);
                //простые не выводить
                fout<<result<<endl;
            }
            isErrorLine = false;
            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1 && isErrorLine == false){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

int dixon(long long n, long long k){
    int base[k];
    base[0] = 2;
    base[1] = 3;
    for(int i = 2; i<k; i++){
        base[i] = base[i-1];
        bool isFine = false;
        while(!isFine){
            bool isFineTemp = true;
            base[i] += 2;
            for(int j = 1; j<i; j++){
                if(base[i] % base[j] == 0){
                    isFineTemp = false;
                    break;
                }
            }
            if(isFineTemp) isFine = true;
        }
    }
    int t = k + 1;
    vector<long long> a;
    vector<long long> b;
    for(int i = 0; i<t; i++){
        a.push_back(0);
        b.push_back(0);
    }
    vector<vector<int>> e(t, vector<int>(k));

    for(int i = 0; i<t; i++){
        long long tempB;
        do{
            a[i] = rand() % n;
            b[i] = (a[i] * a[i]) % n;
            for(int j = 0; j < k; j++){
                e[i][j] = 0;
            }
            tempB = b[i];
            if(b[i] != 0){
                for(int j = 0; j < k; j++){
                    while(tempB % base[j] == 0){
                        e[i][j]++;
                        tempB = tempB / base[j];
                    }
                }
            } else {
                tempB = 1000; //если b[i] получилось равно нулю, то надо просто новую a[i] придумать
            }
        } while (tempB != 1);
    }

step3:
    int V[t][k];
    for(int i = 0; i<t; i++){
        for(int j = 0; j<k; j++){
            V[i][j] = e[i][j] % 2;
        }
    }
    int c[t];
    int temp = 1;
    while(temp < pow(2, t)){
        for(int i = t-1; i>=0; i--){
            c[i] = 0;
            c[i] = (temp>>i) & 1;
        }
        bool isSolution = true;
        for(int i = 0; i < k; i++){
            int sum = 0;
            for(int j = 0; j < t; j++){
                sum += c[j]*V[j][i];
            }
            if(sum % 2 != 0) isSolution = false;
        }
        if(isSolution){
            int x = 1;
            int y = 1;
            for(int i = 0; i < t; i++){
                if(c[i] == 1){
                    x *= a[i];
                }
            }
            x = x % n;
            for(int j = 0; j<k; j++){
                int p = 0;
                for(int i = 0; i<t; i++){
                    if(c[i] == 1){
                        p += e[i][j];
                    }
                }
                p = p/2;
                y *= pow(base[j], p);
            }
            if((x % n) != (y % n) && (x+y) % n != 0){
                cout<<"got results ("<< gcd(abs(x-y), n)<<", "<<gcd(abs(x+y), n)<<")"<<endl;
                return gcd(x-y, n);
            }
        }
        temp++;
    }
    t++;
    a.push_back(0);
    b.push_back(0);
    e.emplace_back(k);
    long long tempB;
    do{
        a[a.size()-1] = rand() % n;
        b[a.size()-1] = (a[a.size()-1] * a[a.size()-1]) % n;

        for(int j = 0; j < k; j++){
            e[a.size()-1][j] = 0;
        }

        tempB = b[a.size()-1];

        if(b[a.size()-1] != 0){
            for(int j = 0; j < k; j++){
                while(tempB % base[j] == 0){
                    e[a.size()-1][j]++;
                    tempB = tempB / base[j];
                }
            }
        } else {
            tempB = 1000; //если b[i] получилось равно нулю, то надо просто новую a[i] придумать
        }
    } while (tempB != 1);
    goto step3;
}