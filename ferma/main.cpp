#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

string ferma(long long);

int main() {
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while (!fin.eof()) {
        if ((tmp < '0' || tmp > '9') && tmp != ' ' &&
            tmp != '\n') { //ошибка если есть другие знаки, кроме чисел и пробелов
            isErrorLine = true;
        }
        if (tmp == ' ' && !isErrorLine) {
            readingNumber++;
            if (readingNumber > 0) { //проверка на длину строки (в кол-ве чисел)
                isErrorLine = true;
            }
        } else if (tmp == '\n') {//вызываем функцию подсчёта

            if (!isErrorLine && readingNumber == 0) { //тут надо проверить на правильность все числа
                //сначала проверить на четность. Если четное то убираем все двойки
                for (; a[0] % 2 == 0; a[0] /= 2);
                string result = ferma(a[0]);
                if(result.length() != 0){
                    fout << result<<endl;
                }
            }
            isErrorLine = false;
            for (unsigned long long &i : a) i = 0;
            readingNumber = 0;
        } else {
            if (readingNumber <= 1 && !isErrorLine) {
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}


bool Q(int m, unsigned long long x) {
    vector<int> res;
    for(int i = 1; i<m; i++){
        res.push_back((i*i)%m);
    }
    for(int r : res){
        if(x == r) return true;
    }
    return false;
}

int my_mod(long long n, long long m){
    long long k = 100*m;
    for(; n < 0; n += k);
    return n % m;
}

string ferma(long long n) {

    string res = "";
    //формат вывода: "a, b"
    //если x больше границы - ничего не выводить
    long long x = floor(sqrt(n));
    if(x*x == n){
        //cout<<n<<" = "<<x<<" * "<<x<<endl;
        if(x == 1) return "";
        return to_string(x) + " " + to_string(x);
    }

    int m[]{3,4,5};
    int map[3][5];
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < m[i]; j++){
            if(Q(m[i], my_mod((j*j-n),m[i])) || my_mod((j*j-n),m[i]) == 0){
                map[i][j] = 1;
            } else {
                map[i][j] = 0;
            }
        }
    }
    x++;
    int r[3];
    for(int i = 0; i < 3; i++){
        r[i] = x % m[i];
    }
    four:
    bool check_one = true;
    for(int i = 0; i < 3; i++){
        if(map[i][r[i]] == 0) check_one = false;
    }

    if (check_one){
        long long z = x*x-n;
        long long y = floor(sqrt(z));
        if(y*y == z){
            //cout<<n<<" = "<<x+y<<" * "<<x-y<<endl;
            if(x+y == 1 || x-y == 1) return "";
            return to_string(x+y) + " " + to_string(x-y);
        }
    }
    x++;
    if(x > (n+9)/6) return "";
    for(int i = 0; i < 3; i++){
        r[i] = (r[i] + 1) % m[i];
    }
    goto four;
}


