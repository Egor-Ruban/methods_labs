#include <iostream>
#include <fstream>

using namespace std;

unsigned long long Karatsuba_method(unsigned long long, unsigned long long);

int main() {

    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') {
            isErrorLine = true;
        }
        if(tmp == ' '){
            readingNumber++;
            if(readingNumber > 1){
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && readingNumber == 1){
                unsigned long long result = Karatsuba_method(a[0], a[1]);
                fout << result << endl;
            }
            isErrorLine = false;
            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

unsigned long long Karatsuba_method(unsigned long long u, unsigned long long v){

    unsigned long long max = v;
    if (u > v) max = u;
    unsigned long long k = 0;
    while(max != 0){
        max >>= 1u;
        k++;
    }
    unsigned long long n = (k%2 == 0) ? (k/2) : (k/2+1);
    unsigned long long mask = (1u << n) - 1;
    unsigned long long u0 = u & mask;
    unsigned long long v0 = v & mask;
    unsigned long long u1 = u >> n;
    unsigned long long v1 = v >> n;

    unsigned long long C = (u1+u0)*(v1 + v0);
    unsigned long long A = (u1*v1);
    unsigned long long B = (u0*v0);
    unsigned long long z = ((A<<(2*n)) + ((C-A-B)<<n) + B);
    return z;
}
