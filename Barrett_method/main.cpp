#include <iostream>
#include <fstream>

using namespace std;

unsigned long long Barrett_method(unsigned long long, unsigned long long);

int main() {
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') {
            isErrorLine = true;
        }
        if(tmp == ' '){
            readingNumber++;
            if(readingNumber > 1){
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && a[1] >= 1){
                try {
                    unsigned long long result = Barrett_method(a[0], a[1]);
                    fout << result << endl;
                }catch(std::exception &e){}
            }
            isErrorLine = false;

            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

//считаем в четверичной системе
unsigned long long Barrett_method(unsigned long long x, unsigned long long m){
    if(x == m) return 0;
    unsigned long long k = 0;
    unsigned long long max = m;
    while(max != 0){
        max >>= 2u;
        k++;
    }


    unsigned long long n = 0;
    max = x;
    while(max != 0){
        max >>= 2u;
        n++;
    }
    if(n > 2*k) throw std::exception();

    unsigned long long z = (1ull<<(4*k))/m;
    unsigned long long tx = x >> 2*(k-1);
    unsigned long long q = (tx * z)>>2*(k+1);
    unsigned long long mask = (1u<<(2*(k+1)))-1;
    unsigned long long r1 = (x & mask);
    unsigned long long r2 = ((q*m) & mask);
   unsigned long long r = 0;
    if(r1 >= r2) r = r1-r2;
    else r = (1u<<2*(k+1)) + r1 - r2;
    while(r >= m){
        r -= m;
    }
    return r;
}
