#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <numeric>
#include <vector>
#include <array>
#include <algorithm>

using namespace std;

int pollard(int, int);

bool isPrime(unsigned long a){
    unsigned long i;
    if (a == 2)
        return true;
    if (a == 0 || a == 1 || a % 2 == 0)
        return false;
    for(i = 3; i*i <= a && a % i; i += 2)
        ;
    return i*i > a;
}

int main() {
    srand(time(nullptr));
    ifstream fin;
    ofstream fout;
    fout.open("output.txt"); //
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') { //ошибка если есть другие знаки, кроме чисел и пробелов
            isErrorLine = true;
        }
        if(tmp == ' ' && !isErrorLine){
            readingNumber++;
            if(readingNumber > 1){ //проверка на длину строки (в кол-ве чисел)
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && readingNumber == 1 && a[1] < a[0] && isPrime(a[0])){ //тут надо проверить на правильность все числа
                int result = pollard(a[0], a[1]);
                if(result != -1){
                    fout<<result<<endl;
                }
            }
            isErrorLine = false;
            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 1 && !isErrorLine){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

int findGenerator(int n){
    int p = n-1;
    vector<int> d;
    if(p % 2 == 0){
        d.push_back(2);
        while(p % 2 == 0) p /= 2;
    }
    for(int i = 3; i < n && p != 1; i += 2){
        if(p % i == 0){
            d.push_back(i);
            while(p % i == 0) p /= i;
        }
    }
    for(int i = 2; i<n; i++){
        bool isGenerator = true;
        for(int q : d){
            int k = 1;
            for(int j = 1; j<=((n-1)/q); j++){
                k = k * i % n;
            }
            if(k % n == 1) isGenerator = false;
        }
        if(isGenerator) return i;
    }
    return 0;
}

int nextX(int x, int a, int n, int g){
    if(x % 3 == 1){
        return (a*x) % n;
    } else if(x % 3 == 2){
        return (x*x) % n;
    } else{
        return (g*x) % n;
    }
}

int nextY(int y, int x, int n){
    if(x % 3 == 1){
        return (y + 1) % (n - 1);
    } else if(x % 3 == 2){
        return (2 * y) % (n-1);
    } else{
        return (y) % (n-1);
    }
}

int nextZ(int z, int x, int n){
    if(x % 3 == 1){
        return (z) % (n-1);
    } else if(x % 3 == 2){
        return (2 * z) % (n-1);
    } else{
        return (z + 1) % (n - 1);
    }
}

struct XYZ{
    int x;
    int z;
    int y;
    XYZ(int xn, int yn, int zn) {
        x = xn;
        y = yn;
        z = zn;
    }
};

XYZ F(XYZ xyz, int a, int g, int n){
    XYZ res(0, 0, 0);
    res.x = nextX(xyz.x, a, n, g);
    res.y = nextY(xyz.y, xyz.x, n);
    res.z = nextZ(xyz.z, xyz.x, n);
    return res;
}

int pollard(int n, int a) {
    XYZ one(1, 0, 0);
    XYZ two(1, 0, 0);

    int g = findGenerator(n);
    do{
        one = F(one, a, g, n);
        two = F(F(two, a, g, n), a, g, n);
    } while (one.x != two.x);
    if(one.y == two.y){
        cout<<"error"<<std::endl;
        return -1;
    } else {
          int d = gcd((n-1 + one.y -  two.y) % (n-1), n-1);
          vector<int> b;
          for(int i = 1; i < n; i++){
              int left = ((n-1 + one.y -  two.y) * i) % (n-1);
              int right = (n-1 + two.z -  one.z) % (n-1);
              if(left == right){
                  b.push_back(i);
              }
          }
          for(int i = 0; i<d; i++){
              int k = 1;
              for(int j = 1; j<=b[i]; j++){
                  k = k * g % n;
              }
              if(k == a){
                  return b[i];
              }
          }
    }
    return -1;
}