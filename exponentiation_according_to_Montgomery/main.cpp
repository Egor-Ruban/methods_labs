#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

unsigned long long MR(unsigned long long, unsigned long long, int, int);
unsigned long long MP(unsigned long long, unsigned long long, unsigned long long, int, int);
unsigned long long ME(unsigned long long, unsigned long long, unsigned long long, unsigned long long);

void ThrowIfBInvalid(unsigned long long int b);

int main() {
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    unsigned long long a[]{0,0,0,0}; //массив для чисел из строки
    unsigned long long readingNumber = 0; //индекс числа в строке
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') {
            isErrorLine = true;
        }
        if(tmp == ' '){
            readingNumber++;
            if(readingNumber >= 4){
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && a[1] >= 1 && a[0] < a[2]){ //проверка ограничений на числа
                try {

                    ThrowIfBInvalid(a[3]);
                    unsigned long long result = ME(a[0], a[1], a[2], a[3]);
                    //cout<<a[0]<<a[1]<<a[2]<<a[3]<<result<<endl;
                    fout << result << endl;
                }catch(std::exception &e){}
            }
            isErrorLine = false;

            for(unsigned long long & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber < 4){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

void ThrowIfBInvalid(unsigned long long int b) {
    int bits = 0;
    while (b != 0){
        bits += (b & 1u);
        b >>= 1u;
    }
    if (bits != 1) throw std::exception();
}

unsigned long long ME(unsigned long long x, unsigned long long y, unsigned long long m, unsigned long long b){
    unsigned int power = 1;
    while (pow(2, power) != b) power++;
    unsigned int k = 0;
    while ((m >> power * k) != 0) k++;
    unsigned int n = 0;
    while ((y >> n) != 0) n++;
    unsigned long long R2D2 = 1;
    for(int i = 0; i < 2*k; i++) R2D2 = R2D2 * b % m;
    unsigned long long newX = MP(x, R2D2, m, power, k);
    unsigned long long z = newX;
    for(int i = n-2; i >= 0; i--){
        z = MP(z, z, m, power, k);
        if(((y>>i)&1) == 1) z = MP(z, newX, m, power, k);
    }
    return MR(z, m, power, k);
}

unsigned long long MP(unsigned long long x, unsigned long long y, unsigned long long m, int power, int k){
    int b = pow(2, power);
    int mask = b - 1;
    int newM = 1;
    while((newM * (m & mask)) % b != 1) newM++;
    newM = -newM + b;
    unsigned long long z = 0;
    for(int i = 0u; i < k; i++){
        unsigned long long xi = (x >> power * i) & mask;
        unsigned long long u = ((z & mask) + xi * (y & mask)) * newM % b;
        z = (z + xi*y + u*m) / b;
    }
    if(z > m) z -= m;
    return z;
}

//R is (2^power)^k
unsigned long long MR(unsigned long long x, unsigned long long m, int power, int k){
    int b = pow(2, power);
    int mask = b - 1;
    int newM = 1;
    while((newM * (m & mask)) % b != 1) newM++;
    newM = -newM + b;
    unsigned long long z = x;
    for(int i = 0; i < k ; i++){
        unsigned long long zi = (z >> power * i) & mask;
        int u = (zi * newM) % b;
        z = z + u*m*pow(b, i);
    }
    z = z/(int)(pow(b, k));
    if(z >= m) z -= m;
    return z;
}
