#include <iostream>
#include <fstream>

using namespace std;

unsigned long long from_left_to_right(int, int, int);

int main() {
    ifstream fin;
    ofstream fout;
    fout.open("output.txt");
    fin.open("input.txt"); // открыли файл для чтения
    if (!fin.is_open() || !fout.is_open()) // если файл не открыт
        return 0;
    int a[]{0,0,0};
    int readingNumber = 0;
    char tmp;
    fin.read(&tmp, 1);
    bool isErrorLine = false;
    while(!fin.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') {
            isErrorLine = true;
        }
        if(tmp == ' '){
            readingNumber++;
            if(readingNumber > 2){
                isErrorLine = true;
            }
        } else if(tmp == '\n'){//вызываем функцию подсчёта
            if(!isErrorLine && a[2] != 0){
                unsigned long long result = from_left_to_right(a[0], a[1], a[2]);
                fout << result << endl;
            }
            isErrorLine = false;
            for(int & i : a) i = 0;
            readingNumber = 0;
        } else {
            if(readingNumber <= 2){
                a[readingNumber] = a[readingNumber] * 10 + (tmp - '0');
            }
        }
        fin.read(&tmp, 1);
    }
    fout.close();
    fin.close();
    return 1;
}

unsigned long long from_left_to_right(int x, int y, int m){
    if(y == 0) return 1;
    //cout<<x<<" "<<y<<" "<<m<<endl;
    unsigned long long z = x;
    unsigned long long t = 2;
    unsigned long long n = 0;
    for(; y >= t;  n++) t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z) % m;
        if (((y >> i) & 1) == 1) z = (z * (x % m)) % m;
    }
    //cout<<x<<" "<<y<<" "<<m<<" "<<z<<endl;
    return z;
}
